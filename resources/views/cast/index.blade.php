@extends('adminlte.master')

@section('content-header')  
Cast Page
@endsection

@section('judul')
<p>List Cast Page</p>
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary my-2">Tambah Cast</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Nama</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>
                            <form action="/cast/{{$value->id}}" method="POST">
                            <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        @endsection