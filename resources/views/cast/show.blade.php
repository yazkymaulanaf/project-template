@extends('adminlte.master')

@section('content-header')  
Show Data Page
@endsection

@section('judul')
<p>Detail Cast</p>
@endsection

@section('content')
<!-- menampilkan data dari db -->
<h2 class="text-primary">Show Cast {{$cast->id}}</h2>
<h4>Nama : {{$cast->nama}}</h4>
<p>Umur : {{$cast->umur}}</p>
<p>Bio : {{$cast->bio}}</p>

<a href="/cast" class="btn btn-outline-dark"> Kembali</a>
@endsection
