<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    // public function create berfungsi menreturn link menuju resources/views/post/create.blade.php
    // public function store(Request $request) berfungsi mengambil request inputan attibute name pada resources/views/post/create.blade.php


    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }
        //code diatas ini berfungsi melakukan insert data pada kolom title dan body berdasarkan request inputan attribute name ‘title’ dan ‘body’ seperti pada sintax sql INSERT INTO post (title, body)
        // VALUES ($request[‘title’], $request[‘body’]);
        // setelah proses insert berhasil maka akan menuju ke URL /post


    
    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }

    public function show($id)
    {
        //CODE dibawah berfungsi memanpilkan data berdasarkan $id yang terdapat pada parameter public funtion show($id) sintax sqlnya
        // SELECT * FROM post WHERE id = $id
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    
    public function edit($id)
    {
        // function edit
        // $cast berfungsi membuat variabel yang menampung fungsi mengambil semua data berdasarkan paramenter variabel di pada public function edit($id) sintax sqlnya SELECT * FROM post WHERE id = $id
        // dan menreturn menuju resources/views/post/edit.blade.php beserta melempar variabel post
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        // berfungsi melakukan validasi jika title kosong atau title tidak boleh sama dengan data yang telah terinput maka akan menampilkan error dan jika body inputan kosong maka akan menampilkan error

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        // syntax dibawah ini berfungsi melakukan update pada inputan attribute name title dan body berdasarkan id pada kolom database
        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        //code dibawah berfungsi menghapus data pada table post berdasarkan $id
        // sintax sqlnya
        // DELETE FROM post WHERE id = $id

        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }

}

    